﻿using CrossPlatform.UI;
using System;

namespace CrossPlatform
{
    static class Program
    {
        [STAThread]
        static int Main(string[] args)
        {
            ApplicationWrapper.Initialize();

            Startup.StartupApplication();

            ApplicationWrapper.Run();

            Environment.Exit(0);

            return 0;

        }

    }
}
