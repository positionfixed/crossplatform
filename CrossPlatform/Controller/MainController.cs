﻿using CrossPlatform.Shared.Helper;
using CrossPlatform.Views;

namespace CrossPlatform.Controller
{
    public class MainController
    {
        private IMainView view;

        public MainController(IMainView view)
        {
            this.view = view;         
        }

        public void InitializeApplication()
        {
            view.SetPlatform("You are running on " + PlatformHelper.RunningPlatform().ToString());
        }
    }
}
