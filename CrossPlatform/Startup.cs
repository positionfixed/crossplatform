﻿using CrossPlatform.Controller;
using CrossPlatform.UI;
using CrossPlatform.Views;

namespace CrossPlatform
{
    public static class Startup
    {
        public static void StartupApplication()
        {
            IMainView mainForm = ApplicationWrapper.CreateMainForm();
            MainController mainController = new MainController(mainForm);

            mainController.InitializeApplication();
        }
    }
}
