﻿namespace CrossPlatform.Shared.Enums
{
    public enum Platform
    {
        Windows,
        Linux,
        Mac
    }
}
