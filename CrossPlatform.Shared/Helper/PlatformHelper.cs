﻿using CrossPlatform.Shared.Enums;
using System;
using System.Runtime.InteropServices;

namespace CrossPlatform.Shared.Helper
{
    public class PlatformHelper
    {
        [DllImport("libc")]
        static extern int uname(IntPtr buf);

        public static Platform RunningPlatform()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    {
                        IntPtr buf = IntPtr.Zero;
                        try
                        {
                            buf = Marshal.AllocHGlobal(8192);
                            // This is a hacktastic way of getting sysname from uname ()
                            if (uname(buf) == 0)
                            {
                                string os = Marshal.PtrToStringAnsi(buf);
                                if (os == "Darwin")
                                    return Platform.Mac;
                            }
                        }
                        catch
                        {
                        }
                        finally
                        {
                            if (buf != IntPtr.Zero)
                                Marshal.FreeHGlobal(buf);
                        }
                        return Platform.Linux;
                    }

                case PlatformID.MacOSX:
                    return Platform.Mac;

                default:
                    return Platform.Windows;

            }
        }
    }
}
