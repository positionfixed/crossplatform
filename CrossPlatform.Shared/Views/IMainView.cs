﻿namespace CrossPlatform.Views
{
    public interface IMainView
    {
        void SetPlatform(string platform);
    }
}
