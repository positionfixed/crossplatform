﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Views
{
    public interface IRecentMultiTestListEntryView : IView
    {
        string URL { set; get; }
        string[] Browsers { get; set; }
        string[] BrowserNames { set; }
        Image[] BrowserIcons { set; }
        event EventHandler TestStarted;
    }
}
