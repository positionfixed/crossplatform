﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using AppKit;
using CrossPlatform.Controller;
using CrossPlatform.UI;
using CrossPlatform;

namespace CrossPlatform.Mac
{
	static class MainClass
	{
		[STAThread]
		public static int Main(string[] args)
		{
            ApplicationWrapper.InitializeMac();

            Startup.StartupApplication();

            ApplicationWrapper.Run();

            return 0;
        }
}
}
