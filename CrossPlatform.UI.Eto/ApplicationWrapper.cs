﻿using CrossPlatform.UI.Eto.Forms;
using CrossPlatform.Views;
using Eto;
using Eto.Forms;

namespace CrossPlatform.UI
{
    public static class ApplicationWrapper
    {
        private static Application app;
        private static Form mainForm;

        public static void Initialize()
        {
			app = new Application();
        }

		public static void InitializeMac()
		{
			app = new Application(Platforms.XamMac2);
		}

        public static IMainView CreateMainForm()
        {
            mainForm = new MainForm();
            return (IMainView)mainForm;
        }

        public static void Run()
        {
            app.Run(mainForm);
        }
    }
}
