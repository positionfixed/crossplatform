﻿using CrossPlatform.Views;
using Eto.Forms;
using Eto.Serialization.Xaml;

namespace CrossPlatform.UI.Eto.Forms
{
    public class MainForm : Form, IMainView
    {
        public Label PlatformLabel { get; set; }
        public MainForm()
        {
            XamlReader.Load(this);
        }

        public void SetPlatform(string platform)
        {
            PlatformLabel.Text = platform;
        }
    }
}
