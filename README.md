# README #

This project can be used as a template for building .Net / Mono desktop applications using the [Eto.Forms](https://github.com/picoe/Eto) UI kit.

### Prequisits ###

* Visual Studio (Windows)
* Monodevelop (Linux)
* Xamarin Studio and Xamarin.Mac (macOS)
* XCode (macOS)

### How To Build ###

On Windows and Linux open CrossPlatform.sln and on macOS use CrossPlatform.Mac.sln

### More information ###

You can get more details about this project on our [blog](https://www.browseemall.com/Blog/index.php/2017/05/23/creating-a-cross-plattform-net-ui-application/).